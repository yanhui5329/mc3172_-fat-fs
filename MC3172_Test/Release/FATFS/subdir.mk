################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../FATFS/ccsbcs.c \
../FATFS/diskio.c \
../FATFS/ff.c 

OBJS += \
./FATFS/ccsbcs.o \
./FATFS/diskio.o \
./FATFS/ff.o 

C_DEPS += \
./FATFS/ccsbcs.d \
./FATFS/diskio.d \
./FATFS/ff.d 


# Each subdirectory must supply rules for building sources it contributes
FATFS/%.o: ../FATFS/%.c
	@	@	riscv-none-embed-gcc -march=rv32imc -mabi=ilp32 -mtune=size -msmall-data-limit=8 -mstrict-align -mno-save-restore -mno-div -mbranch-cost=1 -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin  -g3 -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@	@

