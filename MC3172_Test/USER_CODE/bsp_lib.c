/*
 * bsp_lib.c
 *
 *  Created on: Oct 16, 2022
 *      Author: BYH
 */
#include "./bsp_lib.h"

void meminit(u8* data,u16 len)
{
    for(u16 i = 0;i < len;i++)
    {
        data[i] = 0;
    }
}
void memcopy(u8* dst,u8* src,u16 len)
{
    for(u16 i = 0;i < len;i++)
    {
        dst[i] = src[i];
    }
}

void delay_ms(u16 count)
{
    u32 core_count_value = 0;
    u32 core_count_now = 0;
    u16 i = 0;
    core_count_value = CORE_CNT;
    core_count_now = CORE_CNT;
    for(i = 0;i < count;i++)
    {

        while( (core_count_now >= core_count_value)&&((core_count_now - core_count_value) <= 12000) )//12000---->1ms
        {
            core_count_now = CORE_CNT;
        }
        while( (core_count_now < core_count_value)&&((core_count_now + core_count_value) <= 0xFFFFFFFF) )//12000---->1ms
        {
            core_count_now = CORE_CNT;
        }
        core_count_value = core_count_now;
    }
}
