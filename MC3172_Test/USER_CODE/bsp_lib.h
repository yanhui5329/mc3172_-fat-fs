/*
 * bsp_lib.h
 *
 *  Created on: Oct 16, 2022
 *      Author: BYH
 */

#ifndef USER_CODE_BSP_LIB_H_
#define USER_CODE_BSP_LIB_H_

#include "../MC3172/MC3172.h"

void meminit(u8* data,u16 len);
void memcopy(u8* dst,u8* src,u16 len);
void delay_ms(u16 count);

#endif /* USER_CODE_BSP_LIB_H_ */
