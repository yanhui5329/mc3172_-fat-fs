/*
 * bsp_soft_rtc.c
 *
 *  Created on: Oct 16, 2022
 *      Author: BYH
 */

#include "./bsp_soft_rtc.h"

rtc_struct_t time;

void rtc_init(void)
{
    time.year = 2022;
    time.month = 10;
    time.day = 16;
    time.hour = 10;
    time.min = 30;
    time.sec = 25;
}

void rtc_time_set(rtc_struct_t* time_new)
{
    memcopy((u8*)(&time),(u8*)time_new,sizeof(rtc_struct_t));
}

void rtc_sec_update(void)
{
    if(time.sec == 59)
    {
        time.sec = 0;
        rtc_min_update();
    }
    else
    {
        time.sec++;
    }
}

void rtc_min_update(void)
{
    if(time.min == 59)
    {
        time.min = 0;
        rtc_hour_update();
    }
    else
    {
        time.min++;
    }
}

void rtc_hour_update(void)
{
    if(time.hour == 23)
    {
        time.hour = 0;
        rtc_day_update();
    }
    else
    {
        time.hour++;
    }
}

void rtc_day_update(void)
{
    if((time.month == 2) && ((time.year % 4) == 0))
    {
        if(time.day == 29)
        {
            time.day = 1;
            rtc_month_update();
        }
        else
        {
            time.day++;
        }
    }
    else if((time.month == 2) && ((time.year % 4) != 0))
    {
        if(time.day == 28)
        {
            time.day = 1;
            rtc_month_update();
        }
        else
        {
            time.day++;
        }
    }
    else if((time.month == 4) || (time.month == 6) || (time.month == 9) || (time.month == 11))
    {
        if(time.day == 30)
        {
            time.day = 1;
            rtc_month_update();
        }
        else
        {
            time.day++;
        }
    }
    else
    {
        if(time.day == 31)
        {
            time.day = 1;
            rtc_month_update();
        }
        else
        {
            time.day++;
        }
    }
}

void rtc_month_update(void)
{
    if(time.month == 12)
    {
        time.month = 1;
        time.year++;
    }
    else
    {
        time.month++;
    }
}
