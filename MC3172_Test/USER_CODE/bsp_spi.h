/*
 * bsp_spi.h
 *
 *  Created on: Oct 11, 2022
 *      Author: BYH
 */

#ifndef USER_CODE_BSP_SPI_H_
#define USER_CODE_BSP_SPI_H_

#include "../MC3172/MC3172.h"

#define USER_MODIFY_SPI (1)

#define USER_SPI_COM    (GPCOM9_BASE_ADDR)

void spi_config(u32 gpcom_sel);

void spi_Select(u32 gpcom_sel);
void spi_DisSelect(u32 gpcom_sel);
u8 spi_ReadWriteByte(u32 gpcom_sel,u8 data);
void spi_SetClk(u32 gpcom_sel,u32 clk);
void spi_WriteTest(u32 gpcom_sel,u8 data);

#endif /* USER_CODE_BSP_SPI_H_ */
