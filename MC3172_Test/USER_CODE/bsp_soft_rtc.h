/*
 * bsp_soft_rtc.h
 *
 *  Created on: Oct 16, 2022
 *      Author: BYH
 */

#ifndef USER_CODE_BSP_SOFT_RTC_H_
#define USER_CODE_BSP_SOFT_RTC_H_

#include "../MC3172/MC3172.h"
#include "bsp_lib.h"

typedef struct rtc_struct
{
    u16 year;
    u8 month;
    u8 day;
    u8 hour;
    u8 min;
    u8 sec;
} rtc_struct_t;

extern rtc_struct_t time;

void rtc_init(void);
void rtc_time_set(rtc_struct_t* time_new);
void rtc_sec_update(void);
void rtc_min_update(void);
void rtc_hour_update(void);
void rtc_day_update(void);
void rtc_month_update(void);



#endif /* USER_CODE_BSP_SOFT_RTC_H_ */
