/*
 * bsp_spi.c
 *
 *  Created on: Oct 11, 2022
 *      Author: BYH
 */
#include "./bsp_spi.h"

void spi_config(u32 gpcom_sel)
{
    INTDEV_SET_CLK_RST(gpcom_sel,(INTDEV_RUN|INTDEV_IS_GROUP0|INTDEV_CLK_IS_CORECLK_DIV2));

    GPCOM_SET_IN_PORT(gpcom_sel,(GPCOM_MASTER_IN_IS_P3));
    GPCOM_SET_OUT_PORT(gpcom_sel,( \
            GPCOM_P0_OUTPUT_ENABLE|GPCOM_P1_OUTPUT_ENABLE|GPCOM_P2_OUTPUT_ENABLE|GPCOM_P3_OUTPUT_DISABLE| \
            GPCOM_P0_IS_HIGH|      GPCOM_P1_IS_MASTER_CLK|GPCOM_P2_IS_MASTER_OUT|GPCOM_P3_IS_HIGH
                      ));

    GPCOM_SET_COM_MODE(gpcom_sel,(GPCOM_SPI_MASTER_MODE3|GPCOM_TX_MSB_FIRST|GPCOM_RX_MSB_FIRST));

    GPCOM_SET_COM_SPEED(gpcom_sel,24000000,300000);//1000000---->500ns,,,200000---->2.5us,,,300000---->1.7us

    GPCOM_SET_OVERRIDE_GPIO(gpcom_sel, ( \
            GPCOM_P0_OVERRIDE_GPIO| \
            GPCOM_P1_OVERRIDE_GPIO| \
            GPCOM_P2_OVERRIDE_GPIO| \
            GPCOM_P3_OVERRIDE_GPIO|GPCOM_P3_INPUT_ENABLE  \
                                              ));
}
void spi_Select(u32 gpcom_sel)
{
    GPCOM_SET_OUT_PORT(gpcom_sel,( \
            GPCOM_P0_OUTPUT_ENABLE|GPCOM_P1_OUTPUT_ENABLE|GPCOM_P2_OUTPUT_ENABLE|GPCOM_P3_OUTPUT_DISABLE| \
            GPCOM_P0_IS_LOW|      GPCOM_P1_IS_MASTER_CLK| GPCOM_P2_IS_MASTER_OUT|GPCOM_P3_IS_HIGH
                      ));
}
void spi_DisSelect(u32 gpcom_sel)
{
    GPCOM_SET_OUT_PORT(gpcom_sel,( \
            GPCOM_P0_OUTPUT_ENABLE|GPCOM_P1_OUTPUT_ENABLE|GPCOM_P2_OUTPUT_ENABLE|GPCOM_P3_OUTPUT_DISABLE| \
            GPCOM_P0_IS_HIGH|      GPCOM_P1_IS_MASTER_CLK| GPCOM_P2_IS_MASTER_OUT|GPCOM_P3_IS_HIGH
                      ));
}

u8 spi_ReadWriteByte(u32 gpcom_sel,u8 data)
{
    u8 rx_data_temp = 0;

    static u8 tx_data_wp=0;
    static u8 rx_data_rp=0;

    tx_data_wp=GPCOM_GET_TX_WP(gpcom_sel);
    rx_data_rp=GPCOM_GET_RX_WP(gpcom_sel);

    GPCOM_SEND_TX_DATA(gpcom_sel,tx_data_wp+0,data);

    tx_data_wp+=1;
    tx_data_wp&=0xf;
    GPCOM_SEND_TX_WP(gpcom_sel,tx_data_wp);
    while(!(GPCOM_TX_FIFO_EMPTY(gpcom_sel))){};

    while(rx_data_rp!=GPCOM_GET_RX_WP(gpcom_sel)){
        rx_data_temp=GPCOM_GET_RX_DATA(gpcom_sel,rx_data_rp);
        rx_data_rp+=1;
        rx_data_rp&=0xf;

    };

    return rx_data_temp;
}

void spi_SetClk(u32 gpcom_sel,u32 clk)
{
    GPCOM_SET_COM_SPEED(gpcom_sel,24000000,clk);
}

void spi_WriteTest(u32 gpcom_sel,u8 data)
{
    spi_Select(gpcom_sel);
    spi_ReadWriteByte(gpcom_sel,data);
    spi_DisSelect(gpcom_sel);

}
