
#include "../FATFS/diskio.h"
#include "../USER_CODE/bsp_sd/bsp_sd.h"
#include "../USER_CODE/bsp_soft_rtc.h"
//#include "malloc.h"
//#include "./User/bsp_rtc/bsp_rtc.h"

#define SD_CARD  0  //SD卡,卷标为0
#define EX_FLASH 1  //外部flash,卷标为1

#define FLASH_SECTOR_SIZE   512

//初始化磁盘
DSTATUS disk_initialize (
    BYTE pdrv               /* Physical drive nmuber (0..) */
)
{
    u8 res=0;
    switch(pdrv)
    {
        case SD_CARD://SD卡
            res = SD_Initialize();//SD_Initialize()
            if(res)//STM32 SPI的bug,在sd卡操作失败的时候如果不执行下面的语句,可能导致SPI读写异常
            {
                SD_SPI_ReadWriteByte(0xff);//提供额外的8个时钟
            }
            break;
        case EX_FLASH://外部flash
            break;
        default:
            res=1;
    }
    if(res)return  STA_NOINIT;
    else return 0; //初始化成功
}

//获得磁盘状态
DSTATUS disk_status (
    BYTE pdrv       /* Physical drive nmuber (0..) */
)
{
    return 0;
}

//读扇区
//drv:磁盘编号0~9
//*buff:数据接收缓冲首地址
//sector:扇区地址
//count:需要读取的扇区数
DRESULT disk_read (
    BYTE pdrv,      /* Physical drive nmuber (0..) */
    BYTE *buff,     /* Data buffer to store read data */
    DWORD sector,   /* Sector address (LBA) */
    UINT count      /* Number of sectors to read (1..128) */
)
{
    u8 res=0;
    if (!count)return RES_PARERR;//count不能等于0，否则返回参数错误
    switch(pdrv)
    {
        case SD_CARD://SD卡
            res=SD_ReadDisk(buff,sector,count);
            if(res)//STM32 SPI的bug,在sd卡操作失败的时候如果不执行下面的语句,可能导致SPI读写异常
            {
                SD_SPI_ReadWriteByte(0xff);//提供额外的8个时钟
            }
            break;
        case EX_FLASH://外部flash
            res=0;
            break;
        default:
            res=1;
    }
   //处理返回值，将SPI_SD_driver.c的返回值转成ff.c的返回值
    if(res==0x00)return RES_OK;
    else return RES_ERROR;
}

//写扇区
//drv:磁盘编号0~9
//*buff:发送数据首地址
//sector:扇区地址
//count:需要写入的扇区数
#if _USE_WRITE
DRESULT disk_write (
    BYTE pdrv,          /* Physical drive nmuber (0..) */
    const BYTE *buff,   /* Data to be written */
    DWORD sector,       /* Sector address (LBA) */
    UINT count          /* Number of sectors to write (1..128) */
)
{
    u8 res=0;
    if (!count)return RES_PARERR;//count不能等于0，否则返回参数错误
    switch(pdrv)
    {
        case SD_CARD://SD卡
            res=SD_WriteDisk((u8*)buff,sector,count);
            break;
        case EX_FLASH://外部flash
            res=0;
            break;
        default:
            res=1;
    }
    //处理返回值，将SPI_SD_driver.c的返回值转成ff.c的返回值
    if(res == 0x00)return RES_OK;
    else return RES_ERROR;
}
#endif


//其他表参数的获得
 //drv:磁盘编号0~9
 //ctrl:控制代码
 //*buff:发送/接收缓冲区指针
#if _USE_IOCTL
DRESULT disk_ioctl (
    BYTE pdrv,      /* Physical drive nmuber (0..) */
    BYTE cmd,       /* Control code */
    void *buff      /* Buffer to send/receive control data */
)
{
    DRESULT res;
    if(pdrv==SD_CARD)//SD卡
    {
        switch(cmd)
        {
            case CTRL_SYNC:
                SD_CS_L;
                if(SD_WaitReady()==0)res = RES_OK;
                else res = RES_ERROR;
                SD_CS_H;
                break;
            case GET_SECTOR_SIZE:
                *(WORD*)buff = 512;
                res = RES_OK;
                break;
            case GET_BLOCK_SIZE:
                *(WORD*)buff = 8;
                res = RES_OK;
                break;
            case GET_SECTOR_COUNT:
                *(DWORD*)buff = SD_GetSectorCount();
                res = RES_OK;
                break;
            default:
                res = RES_PARERR;
                break;
        }
    }
    else res=RES_ERROR;//其他的不支持
    return res;
}
#endif
//获得时间
//User defined function to give a current time to fatfs module      */
//31-25: Year(0-127 org.1980), 24-21: Month(1-12), 20-16: Day(1-31) */
//15-11: Hour(0-23), 10-5: Minute(0-59), 4-0: Second(0-29 *2) */
DWORD get_fattime (void)
{
		return	  ((DWORD)(time.year - 1980) << 25)	/* Year 2015 */
			| ((DWORD)time.month << 21)				/* Month 1 */
			| ((DWORD)time.day << 16)				/* Mday 1 */
			| ((DWORD)time.hour << 11)				/* Hour 0 */
			| ((DWORD)time.min << 5)				  /* Min 0 */
			| ((DWORD)time.sec >> 1);				/* Sec 0 */
}


